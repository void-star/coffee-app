/**
 *  Authors: Haryadi Herdian, Nouras Fatima, Matthew Signorini
 */

package htcp2.utils;

import java.util.HashMap;
import java.util.Set;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.io.*;


/**
 *  A class to store data about a single item that a customer is
 *  ordering.
 */
public class OrderItem
{
    private HashMap <String, String> attributes;
    private Set <String> keys;

    /**
     *  Initialise a new, empty order item.
     */
        public
    OrderItem ()
    {
        initMapping ();
    }

    /**
     *  Initialise a new order item, given a list of parameters stored in
     *  a URL encoded string.
     */
        public
    OrderItem (String paramList)
    {
        String [] pair; 
        String name, value;

        // initialise the hash mapping.
        initMapping ();

        // the URL encoded parameter list is a sequence of name=value
        // pairs, separated by & chars.
        for (String param : paramList.split ("&"))
        {
            pair = param.split ("=");

            if (pair.length != 2)
                continue;

            addAttributeFromURLEncodedPair (pair [0], pair [1]);
        }
    }

    /**
     *  Decode a URL encoded attribute name and value, and store the
     *  mapping between the attribute name and its value.
     */
        private void
    addAttributeFromURLEncodedPair (String encodedName, String encodedValue)
    {
        try
        {
            // decode the URL encoded name and value.
            String name = URLDecoder.decode (encodedName, "UTF-8");
            String value = URLDecoder.decode (encodedValue, "UTF-8");

            // add the param to the mapping.
            attributes.put (name, value);
        }
        catch (UnsupportedEncodingException e)
        {
            System.err.println ("Error: I need UTF-8!");
            e.printStackTrace ();
        }
    }

    /**
     *  Initialise the hash map.
     */
        private void
    initMapping ()
    {
        attributes = new HashMap <String, String> ();
        keys = attributes.keySet ();
    }

    /**
     *  Return a list of all the attribute names, as an array of strings.
     */
        public String []
    getAttributeNames ()
    {
        return keys.toArray (new String [keys.size ()]);
    }

    /**
     *  Get the value for a given attribute of an item. If there is no
     *  attribute that matches the name, this method will return an empty
     *  string.
     */
        public String
    getValue (String paramName)
    {
        String value = attributes.get (paramName);

        if (value == null)
            value = "";

        return value;
    }

    /**
     *  Convert the item to a URL encoded string. This method returns the
     *  URL encoded string, using the UTF-8 charset.
     */
        public String
    toUrlEncoded ()
    {
        String encoded = "", value;

        // step through all the valid names in the hash map.
        for (String name : keys.toArray (new String [keys.size ()]))
        {
            value = attributes.get (name);
            encoded = addPairToURLEncodedBuffer (encoded, name, value);
        }

        return encoded;
    }

    /**
     *  Converts a name and value to URL encoded text, and appends a pair
     *  name=value to a buffer of such pairs, separated with ampersand "&"
     *  characters. Return value is the buffer with the new pair added.
     */
        private String
    addPairToURLEncodedBuffer (String buffer, String name, String value)
    {
        try
        {
            String encodedName = URLEncoder.encode (name, "UTF-8");
            String encodedValue = URLEncoder.encode (value, "UTF-8");

            // if there are other pairs before this one, add an
            // ampersand to separate the pairs.
            if (buffer.equals ("") != true)
                buffer += "&";

            buffer += encodedName + "=" + encodedValue;
        }
        catch (UnsupportedEncodingException e)
        {
            System.err.println ("Error: I need UTF-8!");
            e.printStackTrace ();
        }

        return buffer;
    }
}

// vim: ts=4 sw=4 et
