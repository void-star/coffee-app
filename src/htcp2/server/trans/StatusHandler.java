// Authors: H. Herdian, N. Fatima and M. Signorini

package htcp2.server.trans;

import java.io.IOException;

import static htcp2.utils.Const.*;
import htcp2.utils.*;
import htcp2.server.*;


/**
 *  Handler for the HTCP2 STATUS command, which lists orders which are
 *  pending commit.
 */
public class StatusHandler implements TransactionCommand
{
    private static final String name = "STATUS";
    private ClientSocket client;
    private StagingArea orders;

    /**
     *  Initialise the handler.
     */
        public
    StatusHandler (ClientSocket client, StagingArea orders)
    {
        this.client = client;
        this.orders = orders;
    }

    /**
     *  Carry out the STATUS command. This method will fetch a listing of
     *  all the orders in the staging area, including the unique id's that
     *  the server assigns to each one, and send the details back in a
     *  multi-line reply.
     */
        public void
    handler (String line) throws IOException
    {
        String replyBuffer = "";
        boolean isFirstOrder = true;

        client.writeLine (REPLY_OK + "Listing follows:");

        // step through each order in the staging area.
        for (RemoteOrder r : orders.listOrders ())
        {
            replyBuffer = appendOrderToBuffer (replyBuffer, r);
        }

        // the multi-line reply is terminated by a line with a single
        // "." char on it.
        client.writeLine (replyBuffer + ".");
    }

    /**
     *  Append an order listing for a single order to a buffer of listings.
     *  @return The buffer with the new listing appended.
     */
        private String
    appendOrderToBuffer (String buffer, RemoteOrder toList)
    {
        // individual orders in the listing are separated by empty lines.
        if (buffer.equals ("") != true)
            buffer += CRLF;

        // add the order id on it's own line, then the list of items.
        buffer += toList.id + CRLF;
        buffer += toList.order.toString ();

        return buffer;
    }

    /**
     *  Return the command name that this class handles.
     */
        public String
    getName ()
    {
        return name;
    }
}

// vim: ts=4 sw=4 et
