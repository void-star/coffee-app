#!  /bin/bash

#   genCodes.sh
#
#   Generate a number of QR codes which each contain an IP address, a port
#   number, and a sequence of 256 random bytes (read from /dev/urandom).
#
#   PARAMETERS:
#       arg 1   IP address or domain name.
#       arg 2   Port number, in hexadecimal.
#       arg 3   Number of QR codes to generate.
#

magic="c0ffee"
addressVersion="01"
ipAddress=`gethostip -x $1`
let "i = 0"

while [ ${i} != $3 ]
do
    # read 256 bytes of randomness from /dev/urandom. This will be used as
    # a shared secret when an Android client connects to the htcp2 server.
    secret=`dd if=/dev/urandom iflag=fullblock bs=256 count=1`

    # convert the flags byte, IP address, and port number from an ASCII
    # representation to binary. This will collectively be the first part of
    # the datagram encoded in the QR code.
    addressBytes=`echo -e "${magic}${addressVersion}${ipAddress}$2" | \
        xxd -p -r`

    # generate the QR code.
    echo -en "${addressBytes}${secret}" | qrencode -8 -l M -o table_${i}.png

    # Store the servers copy of the auth secret.
    echo -en "${secret}" > table_${i}.passwd

    let "i = ${i} + 1"
done

# vim: ts=4 sw=4 et
