#!  /bin/bash

#   A script to start the HTCP2 server on a given port.
#
#   Part of the Quickie Project.
#
#   Authors: H. Herdian, N. Fatima and M. Signorini

# check that we have a port number as a parameter.
if [ $# != 1 ];
then
    echo "You need to tell me what port number to use."
    exit 1
fi

# tell java where to search for our compiled class files.
export CLASSPATH="./bin:${CLASSPATH}"

# start the server.
java htcp2.server.ServerMain --port $1

# vim: ts=4 sw=4 et
